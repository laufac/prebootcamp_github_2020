<?php 

function reemplazar($fichero_r, $fichero_a, $palabra_actual, $palabra_nueva){
    
    if(file_exists($fichero_a)){
        unlink($fichero_a);
    }

    $fdr = fopen($fichero_r, 'r');
    $fda = fopen($fichero_a, 'a');

    while (($contenido = fgets($fdr)) !== false) {
        $linea_nueva = str_replace($palabra_actual, $palabra_nueva, $contenido,);
        fwrite($fda, $linea_nueva);
    }

    fclose($fdr);
    fclose($fda);
}

reemplazar('quijote.txt', 'quijote_nuevo.txt', "Quijote", "Quijota");

?>