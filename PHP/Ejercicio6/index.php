<?php

    function checkPassword($contraseña1, $contraseña2){
        if ((strlen($contraseña1) >= 8) && (strcmp($contraseña1, $contraseña2) == 0)){
            return "Datos procesados correctamente";
        }else{
            return "Error en el formulario";
        }
    }
    
    if(isset($_POST['OK'])){
        echo checkPassword($_POST['password'],$_POST['repassword']);
    }

?>

<!DOCTYPE html>
    <head>
		<meta charset="utf-8">
	</head>
	<body>

        <h1>Formulario de registro - Ejercicio 6 PHP</h1>
            
            <form action="index.php" method="POST">
                <label for="email">
                <p>Introduce tu e-mail</p>
                <input name="email" type="text">
                </label>
                <label for="password">
                <p>Introduce tu contraseña</p>
                <input name="password" type="text">
                </label>
                <label for="repassword">
                <p>Confirma la contraseña</p>
                <input name="repassword" type="text">
                </label>
                <label for="Submit"></label>
                <input type="submit" name = "OK" value="OK" />
                </label>
            </form>

        </form>
           
	</body>
</html>
