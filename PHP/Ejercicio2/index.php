<?php 

function checkVocal($cadena){
$vocales = ['a', 'e','i', 'o','u'];
$count = 0;
$boolean;
    for($i=0; $i<5; $i++){
        if(substr_count($cadena, $vocales[$i]) > 0)
            $count++;
    }

    if ($count==5)
        $boolean = true;
    else
        $boolean = false;

    return $boolean;
}

echo "Introduce una cadena de texto por favor!\n";

if(isset($_POST['OK'])){
    $respuesta = checkVocal($_POST['string']);
    if ($respuesta == true){
        echo "LA PALABRA CONTIENE LAS 5 VOCALES";
    }else{
        echo "NO CONTIENE TODAS LAS VOCALES";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ejercicio 2 PHP</title>
</head>

<body>

<form action="index.php" method="POST"> 
		<input type="text" name="string" />  
		<input type="submit" name = "OK" value="OK" />
</form>

  
</body>

</html>

